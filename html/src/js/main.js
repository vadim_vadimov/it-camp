$(function () {

  	if($('.process__slider').length > 0) {
        $('.process__slider').slick({
            slidesToShow: 1,
            infinite: true,
            dots: true,
            arrows: false,
            speed: 700,
            slidesToScroll: 1,
            fade: true,
            cssEase: 'linear', 
        });
    }


    $('.header__burger').on('click', function () {
      $('.header__menu').addClass('active');
      $('body').addClass('overflowHidden');
    });

    $('.header__menu-close').on('click', function () {
      $('.header__menu').removeClass('active');
      $('body').removeClass('overflowHidden');
    });

    $('.posts-inner__search-close').hide();

    $('.posts-inner__search-btn').on('click', function () {
      $('.posts-inner__search-field').addClass('active');
      $('.posts-inner__nav-list').addClass('hidden');
      $('.posts-inner__search-close').show();
      $(this).hide();
    });

     $('.posts-inner__search-close').on('click', function () {
      $('.posts-inner__search-field').removeClass('active');
      $('.posts-inner__nav-list').removeClass('hidden');
      $('.posts-inner__search-btn').show();
      $(this).hide();
    });



    if ($(window).width() < 739) {
      $('.posts-inner__search-btn').on('click', function () {
        $('.posts-inner__nav-list').removeClass('hidden');
      });


    }


    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      infinite: false,
      asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      arrows: false,
      infinite: false,
      focusOnSelect: true
    });



    function pageWidget(pages) {
            var widgetWrap = $('<div class="widget_wrap"><ul class="widget_list"></ul></div>');
            widgetWrap.prependTo("body");
            for (var i = 0; i < pages.length; i++) {
                $('<li class="widget_item"><a class="widget_link" href="' + pages[i][0] + '.html' + '">' + pages[i][1] + '</a></li>').appendTo('.widget_list');
            }
            var widgetStilization = $('<style>body {position:relative} .widget_wrap{position:absolute;top:0;left:0;z-index:9999;padding:5px 10px;background:#222;border-bottom-right-radius:10px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#222 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_list{padding: 0;}.widget_item{list-style:none;padding:0 0 10px}.widget_link{color:#fff;text-decoration:none;font-size:15px;}.widget_link:hover{text-decoration:underline} </style>');
            widgetStilization.prependTo(".widget_wrap");
        }

        pageWidget([
            ['index', 'Homepage'],
            ['for-employers', 'For Employers'],
            ['for-candidates', 'For Candidates'],
            ['blog', 'Blog'],
            ['blog-single', 'Blog Single'],
            ['contacts', 'Contacts'],
        ]);
                        


  
});