'use strict';

const sass = require('gulp-sass');
const gulp = require('gulp');
const sourcemaps = require('gulp-sourcemaps');
const rigger = require('gulp-rigger');
const autoprefixer = require('gulp-autoprefixer');
const cssmin = require('gulp-cssmin');
const rename = require('gulp-rename');
const replace = require('gulp-replace');
const ftp = require('gulp-ftp');


const path = {
    build: {
        js: 'assets/js/',
        css: 'assets/css/'
    },
    src: {
        js: 'src/js/*.js',
        style: 'src/style/style.scss'
    },
    watch: {
        js: 'src/js/**/*.js',
        style: 'src/style/**/*.scss'
    }
};


function css() {
    return gulp
        .src(path.src.style)
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(rename('style.css'))
        .pipe(replace('../../', '../'))
        .pipe(gulp.dest(path.build.css))
        .pipe(ftp({
            host: '91.237.98.22',
            user: 'developer@itcamp.lv',
            pass: 'hfnNe[vOPh+Q',
            remotePath: '/wp-content/themes/it-camp/assets/css'
        }));
}
function cssMin() {
    return gulp
        .src(path.src.style)
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['> 0.01%'],
            cascade: false
        }))
        .pipe(cssmin())
        .pipe(rename('style.css'))
        .pipe(replace('../../', '../'))
        .pipe(gulp.dest(path.build.css))
}

function js() {
    return gulp
        .src(path.src.js)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.js))
        .pipe(ftp({
            host: '91.237.98.22',
            user: 'developer@itcamp.lv',
            pass: 'hfnNe[vOPh+Q',
            remotePath: '/wp-content/themes/it-camp/assets/js'
        }))
}

function watchFiles() {
    gulp.watch(path.watch.style, css);
    gulp.watch(path.watch.js, js);
}



const styles = gulp.series(css);
const stylesMin = gulp.series(cssMin);
const scripts = gulp.series(js);
const build = gulp.series(css, js)
const watch = gulp.parallel(watchFiles);


exports.styles = styles;
exports.stylesMin = stylesMin;
exports.scripts = scripts;
exports.build = build;
exports.watch = watch;
exports.default = watch;