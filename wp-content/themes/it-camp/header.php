<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no" />
    <meta name="HandheldFriendly" content="true" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_template_directory_uri() ?>/assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <title>
        <?php wp_title(); ?>
    </title>

    <?php wp_head(); ?>
</head>
<body>

<?php if (!is_page_template( 'template-pages/marathon-page.php' )) { ?>


    <?php if (is_page_template( 'template-pages/for-employers.php' )) { ?>
        <div class="intro-block"   style="background-image: url(<?php the_field('intro_employers_bg') ?>);">
            <header class="header">
    <?php } else if (is_page_template( 'template-pages/for-candidates.php' )) { ?>
        <div class="intro-block"   style="background-image: url(<?php the_field('candidates_intro_bg') ?>);">
            <header class="header">
    <?php }else if (is_page_template( 'template-pages/contacts-page.php' )) { ?>
        <div class="intro-block"   style="background-image: url(<?php the_field('contacts_intro_bg') ?>);">
            <header class="header">
    <?php } else if (is_page_template( 'template-pages/about-us-page.php' )) { ?>
        <div class="intro-block"   style="background-image: url(<?php the_field('about_main_bg') ?>);">
            <header class="header">
    <?php } else if (is_page_template( 'template-pages/services-page.php' )) { ?>
        <div class="intro-block"   style="background-image: url(<?php the_field('services_main_bg') ?>);">
            <header class="header">
    <?php } else if (is_page_template( 'template-pages/referral-program-page.php' )) { ?>
        <header class="header  header-background">
    <?php } else if (is_page_template( 'template-pages/candidate-vacancy-page.php' )) { ?>
        <header class="header  header-background">
    <?php } else if (is_page_template( 'template-pages/send-cv-page.php' )) { ?>
        <header class="header  header-background">
    <?php } else if (is_front_page()) { ?>
        <div class="intro-block"   style="background-image: url(<?php the_field('intro_bg') ?>);">
            <header class="header">
    <?php } else if (is_404()) { ?>
            <header class="header  header-404">
    <?php } else if  ( is_post_type_archive( 'blog' )) { ?>
        <div class="intro-block"   style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/img/blog-bg.jpg);">
            <header class="header">
    <?php } else if  ( is_page_template( 'template-pages/blog-page.php' ) ) { ?>
        <div class="intro-block"   style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/img/blog-bg.jpg);">
            <header class="header">
    <?php } else if  ( is_category() ) { ?>
       <div class="intro-block"   style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/img/blog-bg.jpg);">
            <header class="header">
    <?php } else if (is_singular('post')) { ?>
        <header class="header  header-background">
    <?php } else if (is_singular('cv')) { ?>
        <header class="header  header-background">
    <?php } else if (is_singular('vacancy')) { ?>
        <header class="header  header-background">
    <?php } else if (is_search()) { ?>
        <header class="header  header-background">
    <?php } else { ?>
        <header class="header  header-background">
    <?php } ?>

            <div class="container-big">
                <div class="header__burger-menu-content">
                    <button class="header__burger">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/menu-burger.svg" alt="img">
                    </button>

                    <div class="lang-switcher">
                        <?php do_action('wpml_add_language_selector'); ?>
                    </div>
                </div>


                <?php if (get_field('logotype', 'options')) { ?>
                    <a href="<?php echo esc_url( home_url() ); ?>" class="header__logo">
                        <img src="<?php the_field('logotype', 'options') ?>" alt="logo">
                    </a>
                <?php } ?>


                <a href="<?php the_field('book_call_link', 'option'); ?>" target="_blank" class="btn-main"><?php _e('Book a call', 'itcamp'); ?></a>
            </div>
        </header>


            <nav class="header__menu">
                <button class="header__menu-close">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/menu-close.svg" alt="img">
                </button>


		        <?php if (get_field('logo_burger', 'options')) { ?>
                    <div class="header__logo-mobile-wrap">
                        <a href="<?php echo esc_url( home_url() ); ?>" class="header__logo-mobile">
                            <img src="<?php the_field('logo_burger', 'options') ?>" alt="logo">
                        </a>
                    </div>
		        <?php } ?>

		        <?php wp_nav_menu(array(
			        'theme_location'  => 'header_menu',
			        'container'       => null,
			        'menu_class'      => 'header__menu-list',
		        )); ?>

                <a href="<?php the_field('book_call_link', 'option'); ?>" target="_blank" class="btn-main  btn-main--mobile"><?php _e('Book a call', 'itcamp'); ?></a>

		        <?php if( have_rows('footer_social_list', 'options') ): ?>
                    <div class="header__social-list">
				        <?php while( have_rows('footer_social_list', 'options') ): the_row();
					        $text = get_sub_field('footer_social_list_item_name');
					        $link = get_sub_field('footer_social_list_item_link');

					        ?>

                            <a href="<?php echo $link; ?>" target="_blank" class="header__social-item"><?php echo $text; ?></a>

				        <?php endwhile; ?>
                    </div>
		        <?php endif; ?>

            </nav>


<?php } ?>