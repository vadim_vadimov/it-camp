<?php get_header(); ?>
	
	<section class="blog-single">
		<div class="container">

			<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

			    <h2 class="main-title  main-title--text-normal"><?php esc_html( the_title() ) ?></h2>

			<?php endwhile; ?>
			<?php endif; ?> 

			<div class="blog-single__category-wrap">

				<?php
				$cats = get_the_category();
				for ($i = 0; $i < count($cats); $i++) {
					echo '<span class="posts__category">' . $cats[ $i ]->cat_name . '</span>';
				}?>

			</div>
			<div class="blog-single__main-img" style="background-image: url(<?php the_field('blog_main_img') ?>);"></div>
			
			<?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

			    <?php esc_html( the_content() ) ?>

			<?php endwhile; ?>
			<?php endif; ?> 

			<div class="share-panel">
				<strong class="share__title"><?php _e('Share:', 'itcamp'); ?></strong>
				<?php echo do_shortcode('[TheChamp-Sharing]') ?>
			</div>
			<div class="blog-single__articles">
				<h3 class="main-title-h3  main-title-h3--center"><?php _e('All articles', 'itcamp'); ?></h3>

                <?php $current_post_id = get_the_id(); ?>
				<?php $args = array('post_type' => 'post',
									'posts_per_page' => 8,
									'order' => 'DESC') ?>

				<?php $page_index = new WP_Query($args) ?>

				<div class="blog-single__articles-list">
					
					<?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                            <?php if ($current_post_id != get_the_ID()): ?>

                                <div class="blog-single__articles-item-wrap">
                                    <a href="<?php echo esc_url( get_permalink() ); ?>" class="blog-single__articles-item">
                                        <h5><?php echo wp_trim_words( get_the_title(), 3 ); ?></h5>
                                    </a>
                                </div>

                            <?php endif; ?>
						
						<?php endwhile; ?>

					<?php endif; ?>	

				</div>

				<?php wp_reset_postdata(); ?>	

			</div>
		</div>
	</section>

<?php get_footer(); ?>