<?php 
/**
 *	Template name: Contacts Page 
 */

get_header(); ?>

        <div class="intro  intro--inner  intro--inner-small">
            <div class="container">

                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <h1><?php esc_html( the_title() ) ?></h1>

                <?php endwhile; ?>
                <?php endif; ?> 

            </div>
        </div>
    </div>

    <div class="contacts-inner">
        <div class="container">
            <div class="contacts-inner__info">
                <div class="contacts-inner__info-item  contacts-inner__info-item--address">

                    <?php if (get_field('contacts_address')) { ?>
                        <div class="contacts-inner__info-item-img">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/geo-c.svg">
                        </div>
                        <?php the_field('contacts_address') ?>
                    <?php } ?>  

                </div>
                <div class="contacts-inner__info-item">

                    <?php if (get_field('contacts_tel')) { ?>
                        <div class="contacts-inner__info-item-img">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/phone-c.svg">
                        </div>
                        <a href="tel:<?php the_field('contacts_tel') ?>"><?php the_field('contacts_tel') ?></a>
                    <?php } ?>  

                </div>
                <div class="contacts-inner__info-item">

                    <?php if (get_field('contacts_email')) { ?>
                        <div class="contacts-inner__info-item-img">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/email-c.svg">
                        </div>
                        <a href="<?php the_field('contacts_email') ?>"><?php the_field('contacts_email') ?></a>
                    <?php } ?>  

                </div>

                <?php if( have_rows('footer_social_list', 'options') ): ?>   
                    <div class="header__social-list">
                        <?php while( have_rows('footer_social_list', 'options') ): the_row(); 
                            $text = get_sub_field('footer_social_list_item_name');
                            $link = get_sub_field('footer_social_list_item_link');

                            ?>
                            
                            <div class="contacts-inner__social-wrap">
                                <a href="<?php echo $link; ?>" target="_blank" class="header__social-item"><?php echo $text; ?></a>
                            </div>

                        <?php endwhile; ?>  
                    </div>
                <?php endif; ?> 

            </div>
        </div>
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2174.3560448647545!2d24.162886366123747!3d56.97696245438041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2sua!4v1594888087619!5m2!1sru!2sua" width="600" height="482" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>

    <?php get_template_part( 'template-parts/lets-talk-form' ); ?>
    

<?php get_footer(); ?>