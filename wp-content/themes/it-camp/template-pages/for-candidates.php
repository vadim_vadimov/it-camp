<?php 
/**
 *	Template name: Candidates page
 */

get_header(); ?>

        <div class="intro  intro--inner">
            <div class="container">
                
                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <?php esc_html( the_content() ) ?>

                <?php endwhile; ?>
                <?php endif; ?> 

            </div>
        </div>
    </div>

    <section class="process-candidates">
        <div class="container container--process-candidates">

            <?php if (get_field('candidates_process_tile')) { ?>
                <h2 class="main-title  main-title--center"><?php the_field('candidates_process_tile') ?></h2>
            <?php } ?>
            
            <?php if (get_field('candidates_process_sub_title')) { ?>
                <h4 class="main-sub-title  main-sub-title--center"><?php the_field('candidates_process_sub_title') ?></h4>
            <?php } ?>

            <?php if( have_rows('candidates_process_list') ): ?>   
                <div class="process-candidates__list">
                    <?php while( have_rows('candidates_process_list') ): the_row(); 
                        $img = get_sub_field('candidates_process_list_img');
                        $text = get_sub_field('candidates_process_list_text');

                        ?>

                        <div class="process-candidates__item-wrap">
                            <div class="process-candidates__item">
                                <div class="process-candidates__item-img">
                                    <img src="<?php echo $img; ?>">
                                </div>
                                <?php echo $text; ?>
                            </div>
                        </div>

                    <?php endwhile; ?>  
                </div>
            <?php endif; ?> 

        </div>
    </section>

    <section class="network  network-candidates">
        <div class="container">

            <?php if (get_field('candidates_vacancy_title')) { ?>
                <h2 class="main-title"><?php the_field('candidates_vacancy_title') ?></h2>
            <?php } ?>

            <?php if (get_field('candidates_vacancy_sub_title')) { ?>
                <h4 class="main-sub-title"><?php the_field('candidates_vacancy_sub_title') ?></h4>
            <?php } ?>

            <?php $args = array('post_type' => 'vacancy',
                                'posts_per_page' => 20,
                                'order' => 'DESC') ?>

            <?php $page_index = new WP_Query($args) ?>

            <div class="network__list">
            
                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <div class="network__item-wrap">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="network__item">
                            <h4><?php echo esc_html( the_title() ); ?></h4>
                            <div class="network__info">

                                <?php if (get_field('vacancy_geo_location')) { ?>
                                    <span class="network__location"><?php the_field('vacancy_geo_location') ?></span>
                                <?php } ?>  

                            </div>
                            <span class="network-candidates__apply">apply</span>
                        </a>
                    </div>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

            </div>
            <?php wp_reset_postdata(); ?>



	        <?php
            $section_subscription_title = get_field('email_subscription_title', 'options');
            $section_subscription_text = get_field('email_subscription_text', 'options');
            $section_subscription_form_shortcode = get_field('email_subscription_form_shortcode', 'options');

            if ( $section_subscription_title && $section_subscription_form_shortcode ) :
            ?>

                <div class="section-subscription">
                    <div class="form-info__content">
                        <div class="form-info__title-content">

                            <h2 class="form-info__title">
                                <?php echo $section_subscription_title; ?>
                            </h2>

                            <?php if ( $section_subscription_text ): ?>
                                <?php echo $section_subscription_text?>
                            <?php endif; ?>
                        </div>

                        <?php echo do_shortcode($section_subscription_form_shortcode); ?>

                    </div>
                </div>

            <?php
            endif;
	        ?>


        </div>
    </section>

    <section class="looking  looking-candidates">
        <div class="looking__list">
            <a href="<?php echo esc_url( get_page_link( 221 ) ); ?>" class="looking__item">
                <div class="looking__hover-img" style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/img/know.jpg);"></div>
                <h2 class="looking__item-title"><?php _e('KNOW SOMEONE', 'itcamp'); ?></h2>
                <span class="looking__item-text"><?php _e('REALLY GOOD?', 'itcamp'); ?></span>
                <span class="looking__item-description"><?php _e('Get bonus for a referal.', 'itcamp'); ?></span>
                <span class="btn-main"><?php _e('REFER A FRIEND', 'itcamp'); ?></span>
            </a>
            <a href="<?php echo esc_url( get_page_link( 246 ) ); ?>" class="looking__item">
                <div class="looking__hover-img" style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/img/vac.jpg);"></div>
                <h2 class="looking__item-title"><?php _e('DON’T SEE VACANCY', 'itcamp'); ?></h2>
                <span class="looking__item-text"><?php _e('YOU LIKE?', 'itcamp'); ?></span>
                <span class="looking__item-description"><?php _e('Send us your CV we will add you to our database.', 'itcamp'); ?></span>
                <span class="btn-main"><?php _e('SEND A CV', 'itcamp'); ?></span>
            </a>
        </div>
    </section>

    <section class="posts-inner">
        <div class="container">

            <?php if (get_field('candidates_blog_title')) { ?>
                <h2 class="main-title"><?php the_field('candidates_blog_title') ?></h2>
            <?php } ?>

            <?php $args = array('post_type' => 'post',
                                'posts_per_page' => 3,
                                'order' => 'DESC') ?>

            <?php $page_index = new WP_Query($args) ?>

            <div class="posts__list">
     
                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <div class="posts__item-wrap">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="posts__item">
                            <div class="posts__item-img" style="background-image: url(<?php the_field('blog_main_img') ?>);">
                                
                                <?php
                                $cats = get_the_category();
                                for ($i = 0; $i < count($cats); $i++) {
                                    echo '<span class="posts__category">' . $cats[ $i ]->cat_name . '</span>';
                                }?>

                            </div>
                            <div class="posts__item-info">
                                <h4><?php echo esc_html( the_title() ); ?></h4>
                                <?php echo esc_html( the_excerpt() ); ?>
                            </div>
                            <div class="posts__item-arrow">
                                <span class="posts__item-arrow-text"><?php _e('read more', 'itcamp'); ?></span>
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/post-arr.svg">
                            </div>
                        </a>
                    </div>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

            </div>
            <?php wp_reset_postdata(); ?>   

        </div>
    </section>

    <section class="help-panel  help-panel-candidates">
        <div class="container">
            <div class="help-panel__content">
                <div class="help-panel__info">

                    <?php if (get_field('candidates_help_title')) { ?>
                        <h2 class="help-panel__title form-info__title"><?php the_field('candidates_help_title') ?></h2>
                    <?php } ?>

                    <?php if (get_field('candidates_help_text')) { ?>
                        <?php the_field('candidates_help_text') ?>
                    <?php } ?>
                                        
                </div>
                <div class="help-panel__btns">
<!--                    <a href="#" class="help-panel__btn"><span>USE CV  BUILDER</span></a>-->
<!--                    <span class="help-panel__btn-text">or</span>-->
                    <a href="<?php echo esc_url( get_page_link( 197 ) ); ?>#contact-us-form" class="help-panel__btn"><span><?php _e('CREATE A PROFESSIONAL <br>RESUME FOR ME', 'itcamp'); ?></span></a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>