<?php 
/**
 *	Template name: Employers Page 
 */

get_header(); ?>
    
        <div class="intro  intro--inner">
            <div class="container">
                
                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <?php esc_html( the_content() ) ?>

                <?php endwhile; ?>
                <?php endif; ?> 

            </div>
        </div>
    </div>

    <section class="who-we-are">
        <div class="container">

            <?php if (get_field('who_we_are_title')) { ?>
                <h2 class="main-title"><?php the_field('who_we_are_title') ?></h2>
            <?php } ?>  

            <div class="who-we-are__content">

                <?php if (get_field('who_we_are_texts')) { ?>
                    <div class="who-we-are__info">
                        <?php the_field('who_we_are_texts') ?>
                    </div>
                <?php } ?>  

                <?php if (get_field('who_we_are_img')) { ?>
                    <div class="who-we-are__info-img">
                        <img src="<?php the_field('who_we_are_img') ?>">
                    </div>
                <?php } ?>  

            </div>
        </div>
    </section>

    <section class="services">
        <div class="container">

            <?php if (get_field('our_services_title')) { ?>
                <h2 class="main-title  main-title--center"><?php the_field('our_services_title') ?></h2>
            <?php } ?>  

            <?php if (get_field('our_services_sub_title')) { ?>
                <h4 class="main-sub-title  main-sub-title--center"><?php the_field('our_services_sub_title') ?></h4>
            <?php } ?>

            <?php if( have_rows('our_services_list') ): ?>   
                <div class="services__list">
                    <?php while( have_rows('our_services_list') ): the_row(); 
                        $img = get_sub_field('our_services_list_img');
                        $text = get_sub_field('our_services_list_text');

                        ?>

                        <div class="services__item-wrap">
                            <div class="services__item">
                                <div class="services__item-img">
                                    <img src="<?php echo $img; ?>">
                                </div>
                                <?php echo $text; ?>
                            </div>
                        </div>

                    <?php endwhile; ?>  
                </div>
            <?php endif; ?> 

        </div>
    </section>

    <section class="network">
        <div class="container">

            <?php if (get_field('employers_cv_title')) { ?>
                <h2 class="main-title"><?php the_field('employers_cv_title') ?></h2>
            <?php } ?>  

            <?php $args = array('post_type' => 'cv',
                                'posts_per_page' => 20,
                                'order' => 'DESC') ?>

            <?php $page_index = new WP_Query($args) ?>

            <div class="network__list">
            
                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <div class="network__item-wrap">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="network__item">
                            <h4><?php echo esc_html( the_title() ); ?></h4>
                            <div class="network__info">

                                <?php if (get_field('cv_geo_location')) { ?>
                                    <span class="network__location"><?php the_field('cv_geo_location') ?></span>
                                <?php } ?>  

                                <span class="network__note"><?php _e('view candidate', 'itcamp'); ?></span>
                            </div>
                        </a>
                    </div>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

            </div>
            <?php wp_reset_postdata(); ?>   
            
            <?php if (get_field('employers_brief_title')) { ?>
                <h3 class="main-title-h3"><?php the_field('employers_brief_title') ?></h3>
            <?php } ?> 

            <?php if (get_field('employers_brief_text')) { ?>
                <p class="network__brief-description"><?php the_field('employers_brief_text') ?></p>
            <?php } ?> 
            
            <a href="<?php echo esc_url( get_page_link( 248 ) ); ?>" class="btn-main"><?php _e('SEND A BRIEF', 'itcamp'); ?></a>
        </div>
    </section>

    <section class="customers-reviews">
        <div class="container">

            <?php if (get_field('testimonials_title')) { ?>
                <h2 class="main-title  main-title--center"><?php the_field('testimonials_title') ?></h2>
            <?php } ?>  

            <div class="customers-reviews__slider-wrap">

                <?php if( have_rows('testimonials_slider_info') ): ?>   
                    <div class="slider-for">
                        <?php while( have_rows('testimonials_slider_info') ): the_row(); 
                            $text = get_sub_field('testimonials_slider_info_text');
                            $name = get_sub_field('testimonials_slider_info_name');
                            $position = get_sub_field('testimonials_slider_info_company');

                            ?>

                            <div class="customers-reviews__big-slider-slide">
                                <div class="customers-reviews__big-slider-item">
                                    <?php echo $text; ?>
                                    <strong class="customers-reviews__name"><?php echo $name; ?></strong>
                                    <span class="customers-reviews__firm"><?php echo $position; ?></span>
                                </div>
                            </div>

                        <?php endwhile; ?>  
                    </div>
                <?php endif; ?> 

                <?php if( have_rows('testimonials_slider_logos') ): ?>   
                    <div class="slider-nav">
                        <?php while( have_rows('testimonials_slider_logos') ): the_row(); 
                            $img = get_sub_field('testimonials_slider_logos_img');

                            ?>

                            <div class="customers-reviews__small-slider-slide">
                                <div class="customers-reviews__small-slider-item">
                                    <div class="customers-reviews__small-slider-img">
                                        <img src="<?php echo $img; ?>">
                                    </div>
                                </div>
                            </div>

                        <?php endwhile; ?>  
                    </div>
                <?php endif; ?> 
                
            </div>
        </div>
    </section>

    <section class="posts-inner">
        <div class="container">
            
            <?php if (get_field('employers_blog_title')) { ?>
                <h2 class="main-title"><?php the_field('employers_blog_title') ?></h2>
            <?php } ?>
            
            <?php $args = array('post_type' => 'post',
                                'posts_per_page' => 3,
                                'order' => 'DESC') ?>

            <?php $page_index = new WP_Query($args) ?>

            <div class="posts__list">
            
                <?php if ($page_index->have_posts() ) :  while ( $page_index->have_posts() ) : $page_index->the_post();?>

                    <div class="posts__item-wrap">
                        <a href="<?php echo esc_url( get_permalink() ); ?>" class="posts__item">
                            <div class="posts__item-img" style="background-image: url(<?php the_field('blog_main_img') ?>);">
                                
                                <?php
                                $cats = get_the_category();
                                for ($i = 0; $i < count($cats); $i++) {
                                    echo '<span class="posts__category">' . $cats[ $i ]->cat_name . '</span>';
                                }?>

                            </div>
                            <div class="posts__item-info">
                                <h4><?php echo esc_html( the_title() ); ?></h4>
                                <?php echo esc_html( the_excerpt() ); ?>
                            </div>
                            <div class="posts__item-arrow">
                                <span class="posts__item-arrow-text"><?php _e('read more', 'itcamp'); ?></span>
                                <img src="<?php echo get_template_directory_uri() ?>/assets/img/post-arr.svg">
                            </div>
                        </a>
                    </div>
                    
                    <?php endwhile; ?>

                <?php endif; ?> 

            </div>
            <?php wp_reset_postdata(); ?>   
        </div>
    </section>

    <section class="help-panel">
        <div class="container">
            <div class="help-panel__content">

                <?php if (get_field('employers_help_title')) { ?>
                    <h2 class="help-panel__title form-info__title"><?php the_field('employers_help_title') ?></h2>
                <?php } ?>  

                <div class="help-panel__links">
                    <a href="<?php echo get_page_link(197); ?>" class="help-panel__link"><?php _e('Send a message', 'itcamp'); ?></a>
                    <a href="<?php the_field('book_call_link', 'option'); ?>" class="help-panel__link"><?php _e('Request a call', 'itcamp'); ?></a>
                </div>
            </div>
        </div>
    </section>
    

<?php get_footer(); ?>