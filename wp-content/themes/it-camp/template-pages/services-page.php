<?php 
/**
 *	Template name: Services Page
 */

get_header(); ?>

        <div class="intro  intro--inner  intro--inner-small">
            <div class="container">

                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <h1><?php esc_html( the_title() ) ?></h1>

                <?php endwhile; ?>
                <?php endif; ?>

            </div>
        </div>
    </div>



    <section class="services-content">
        <div class="container">

            <?php $services_title = get_field( 'services_title' ); ?>
            <?php if ( $services_title ) : ?>
                <h1 class="services-title">
                    <?php echo $services_title; ?>
                </h1>
            <?php endif; ?>

            <?php $services = get_field( 'services' ); ?>
            <?php if ( $services ) : ?>
                <div class="service-items">

                    <?php foreach ( $services as $service ) : ?>
                        <div class="service-item">
                            <div class="service-item__title">
                                <?php echo $service[ 'title' ]; ?>
                            </div>
                            <div class="service-item__icon">
                                <img src="<?php echo $service[ 'icon' ]; ?>">
                            </div>
                            <div class="service-item__description">
                                <?php echo wpautop( do_shortcode( $service[ 'description' ] ) ); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>

                </div>
            <?php endif; ?>




        </div>
    </section>


    <section class="form-info services-form-info" id="contact-us-form" style="background-image: url(<?php the_field('contact_form_background') ?>);">
        <div class="container">
            <div class="form-info__content">
                <div class="form-info__title-content">

                    <?php $form_title = get_field( 'contact_form_title' ); ?>
                    <?php if ( $form_title ) { ?>
                        <h2 class="form-info__title"><?php echo $form_title; ?></h2>
                    <?php } ?>

                    <?php $form_subtitle = get_field( 'contact_form_subtitle' ); ?>
                    <?php if ( $form_subtitle ) : ?>
                        <?php echo wpautop( $form_subtitle ); ?>
                    <?php endif; ?>
                </div>

                <?php $form_shortcode = get_field( 'contact_form_shortcode' ); ?>
                <?php if ( $form_shortcode ) : ?>
                    <?php echo do_shortcode( $form_shortcode ); ?>
                <?php endif; ?>

            </div>
        </div>
    </section>

    

<?php get_footer(); ?>