<?php 
/**
 *	Template name: Form Page 
 */

get_header(); ?>

    <section class="send-cv">
        <div class="container">

            <?php if (get_field('form_page_title')) { ?>
                <h2 class="main-title  main-title--center"><?php the_field('form_page_title') ?></h2>
            <?php } ?>  
            
            <?php if (get_field('form_page_sub_title')) { ?>
                <p class="send-cv__sub-title"><?php the_field('form_page_sub_title') ?></p>
            <?php } ?>
            
            <?php if (get_field('form_page_form')) { ?>
                <?php the_field('form_page_form') ?>
            <?php } ?> 
        </div>
    </section>


<?php get_footer(); ?>