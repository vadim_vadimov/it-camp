<?php 
/**
 *	Template name: Home Page 
 */

get_header(); ?>

        <div class="intro">
            <div class="container">

                <?php $intro_title = get_field('intro_title'); ?>
                <?php if ($intro_title) { ?>
                    <div class="intro-title">
                        <?php echo $intro_title; ?>

                        <?php $intro_title_slides = get_field('intro_title_slides'); ?>
                        <?php if ($intro_title_slides) {  ?>
                            <div class="intro-title-slider">
                                <?php foreach ($intro_title_slides as $intro_title_slide) { ?>
                                    <div>
                                        <div class="intro-title-slide">
                                            <?php echo $intro_title_slide['text']; ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php  } ?>
                    </div>
                <?php } ?>
 
            </div>

        </div>
    </div>

    <div class="approach">
        <div class="container">

            <?php if (get_field('intro_text')) { ?>
                <?php the_field('intro_text') ?>
            <?php } ?>


	        <?php
	        $intro_link = get_field('intro_btn_link');
	        if( $intro_link ):
		        $intro_link_url = $intro_link['url'];
		        $intro_link_title = $intro_link['title'];
		        $intro_link_target = $intro_link['target'] ? $intro_link['target'] : '_self';
		        ?>
                <a href="<?php echo esc_url( $intro_link_url ); ?>" target="<?php echo esc_attr( $intro_link_target ); ?>" class="link-to"><?php echo esc_html( $intro_link_title ); ?></a>
	        <?php endif; ?>

            <?php if (get_field('intro_btn_text')) { ?>

            <?php } ?>  

        </div>
    </div>

    <section class="process">
        <div class="container">

            <?php if (get_field('our_process_title')) { ?>
                <h2 class="main-title"><?php the_field('our_process_title') ?></h2>
            <?php } ?>  

            <div class="process__content">

                <?php if( have_rows('process_slider') ): ?>   
                    <div class="process__slider">
                        <?php while( have_rows('process_slider') ): the_row(); 
                            $title = get_sub_field('process_slider_title');
                            $text = get_sub_field('process_slider_text');

                            ?>

                            <div class="process__slide">
                                <div class="process__slide-item">
                                    <h4><?php echo $title; ?></h4>
                                    <?php echo $text; ?>
                                </div>
                            </div>

                        <?php endwhile; ?>  
                    </div>
                <?php endif; ?> 

                <div class="process__loader">
                    <svg class="process-slider__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.48 313.65">
                        <path d="M186,193.24V97.08a9.6,9.6,0,0,0-9.6-9.6H61.72a9.6,9.6,0,0,1-9.6-9.6V12.1a9.6,9.6,0,0,1,9.6-9.6H131.6a9.6,9.6,0,0,1,9.6,9.6v207a9.6,9.6,0,0,1-9.6,9.6H17.29a9.6,9.6,0,0,0-9.6,9.6v63.26a9.6,9.6,0,0,0,9.6,9.6H79.52a9.6,9.6,0,0,0,9.6-9.6V168a9.6,9.6,0,0,0-9.6-9.6H0" />
                    </svg>
                    <svg class="process-slider__svg active" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 188.48 313.65">
                        <path d="M186,193.24V97.08a9.6,9.6,0,0,0-9.6-9.6H61.72a9.6,9.6,0,0,1-9.6-9.6V12.1a9.6,9.6,0,0,1,9.6-9.6H131.6a9.6,9.6,0,0,1,9.6,9.6v207a9.6,9.6,0,0,1-9.6,9.6H17.29a9.6,9.6,0,0,0-9.6,9.6v63.26a9.6,9.6,0,0,0,9.6,9.6H79.52a9.6,9.6,0,0,0,9.6-9.6V168a9.6,9.6,0,0,0-9.6-9.6H0" />
                    </svg>
                </div>
            </div>
        </div>
    </section>

    <section class="looking">
        <div class="looking__list">
            <a href="<?php echo esc_url( get_page_link( 177 ) ); ?>" class="looking__item">
                <div class="looking__hover-img" style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/img/job-bg.jpg);"></div>
                <span class="looking__item-text"><?php _e('Looking For', 'itcamp'); ?></span>
                <h2 class="looking__item-title"><?php _e('Jobs', 'itcamp'); ?></h2>
            </a>
            <a href="<?php echo esc_url( get_page_link( 129 ) ); ?>" class="looking__item">
                <div class="looking__hover-img" style="background-image: url(<?php echo get_template_directory_uri() ?>/assets/img/peop-bg.jpg);"></div>
                <span class="looking__item-text"><?php _e('Looking For', 'itcamp'); ?></span>
                <h2 class="looking__item-title"><?php _e('People', 'itcamp'); ?></h2>
            </a>
        </div>
    </section>

    <section class="benefits">
        <div class="container">

            <?php if (get_field('benefits_title')) { ?>
                <h2 class="main-title"><?php the_field('benefits_title') ?></h2>
            <?php } ?>  

            <?php if( have_rows('benefits_list') ): ?>   
                <div class="benefits__list">
                    <?php while( have_rows('benefits_list') ): the_row(); 
                        $img = get_sub_field('benefits_list_img');
                        $text = get_sub_field('benefits_list_text');

                        ?>

                        <div class="benefits__item-wrap">
                            <div class="benefits__item">
                                <div class="benefits__item-img">
                                    <img src="<?php echo $img; ?>">
                                </div>
                                <?php echo $text; ?>
                            </div>
                        </div>

                    <?php endwhile; ?>  

                    <?php if (get_field('benefits_last_item_title')) { ?>
                        <div class="benefits__item-wrap">
                            <div class="benefits__item">
                                <h4><?php the_field('benefits_last_item_title') ?></h4>
                                <a href="#" class="btn-main"><?php _e('Order Service', 'itcamp'); ?></a>
                            </div>
                        </div>
                    <?php } ?>  

                </div>
            <?php endif; ?> 

        </div>
    </section>
    
    <section class="trusted">
        <div class="container">

            <?php if (get_field('trusted_title')) { ?>
                <h2 class="main-title"><?php the_field('trusted_title') ?></h2>
            <?php } ?>  

            <?php if( have_rows('trusted_list') ): ?>   
                <div class="trusted__list">
                    <?php while( have_rows('trusted_list') ): the_row(); 
                        $img = get_sub_field('trusted_list_img');

                        ?>

                        <div class="trusted__item-wrap">
                            <div class="trusted__item">
                                <img src="<?php echo $img; ?>">
                            </div>
                        </div>

                    <?php endwhile; ?>  
                </div>
            <?php endif; ?> 

        </div>
    </section>

    <?php get_template_part( 'template-parts/lets-talk-form' ); ?>

<?php get_footer(); ?>