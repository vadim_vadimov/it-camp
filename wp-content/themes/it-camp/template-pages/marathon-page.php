<?php 
/**
 *	Template name: Marathon Page
 */
?>

<?php get_header(); ?>

    <div class="wrapper">
        <section id="main_section" class="main_section">
            <div class="flex">
                <div class="main_section-description wow fadeIn">

                    <div class="main_section-description-inner">

                        <?php $intro_title = get_field('intro_title'); ?>
                        <?php if ( $intro_title ) : ?>
                            <h1 class="c-white">
                                <?php echo $intro_title; ?>
                            </h1>
                        <?php endif; ?>

                        <?php $intro_subtitle = get_field('intro_subtitle'); ?>
                        <?php if ( $intro_subtitle ) : ?>
                            <p class="main_section-text c-white">
                                <?php echo $intro_subtitle; ?>
                            </p>
                        <?php endif; ?>

                        <div class="participate flex">
                            <?php $intro_btn_text = get_field('intro_button_text'); ?>
                            <?php if ( $intro_btn_text ) : ?>
                                <a href="#register" class="btn text-uppercase scrollTo">
                                    <?php echo $intro_btn_text; ?>
                                </a>
                            <?php endif; ?>


                            <?php
                            $event_date = get_field('event_date');
                            $event_telegram = get_field('event_telegram');
                            ?>
                            <?php if ( $event_date || $event_telegram ) : ?>
                                <div class="main_section-event_info">
                                    <table class="">
                                        <?php if ( $event_date ) : ?>
                                            <tr>
                                                <td>
                                                    <img width="20" height="20" src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/01_Calendar_white.svg" alt="">
                                                </td>
                                                <td>
                                                    <?php echo $event_date; ?>
                                                </td>
                                            </tr>
                                        <?php endif; ?>

                                        <?php if ( $event_telegram ) : ?>
                                            <tr>
                                                <td>
                                                    <img width="22" height="20" src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/02_Telegram_whihe.svg" alt="">
                                                </td>
                                                <td>
                                                    <?php echo $event_telegram; ?>
                                                </td>
                                            </tr>
                                        <?php endif; ?>
                                    </table>
                                </div>
                            <?php endif; ?>

                        </div>

                    </div>

                </div>
                <div class="main_section-image wow fadeIn">

                </div>
            </div>
        </section>

        <section id="release_info" class="release_info">
            <div class="flex container release_info-items">

                <?php
                $highlight_1_title = get_field('highlight_1_title');
                $highlight_1_text = get_field('highlight_1_text');
                $highlight_2_title = get_field('highlight_2_title');
                $highlight_2_text = get_field('highlight_2_text');
                ?>

                <?php if ( $highlight_1_text && $highlight_1_text ) : ?>
                    <div class="release_info-block flex wow fadeIn">
                        <div class="release_info-icon">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/04_Raketa_Big.svg" alt="">
                        </div>
                        <div class="release_info-description">
                            <div class="subtitle">
                                <?php echo $highlight_1_title; ?>
                            </div>
                            <div class="release_info-description">
                                <?php echo $highlight_1_text; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ( $highlight_2_title && $highlight_2_text ) : ?>
                    <div class="release_info-block flex wow fadeIn">
                        <div class="release_info-icon">
                            <img src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/05_Telegram_big.svg" alt="">
                        </div>
                        <div class="release_info-description">
                            <div class="subtitle">
                                <?php echo $highlight_2_title; ?>
                            </div>
                            <div class="description-text c-white">
                                <?php echo $highlight_2_text; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </section>

        <section id="about" class="about">
            <div class="flex container about-content wow fadeIn">
                <div class="about-block">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/Alexandra.jpg" alt="">
                </div>
                <div class="about-block">
                    <?php $about_title = get_field('about_title'); ?>
                    <?php if ( $about_title ) : ?>
                        <h3 class="text-uppercase">
                            <?php echo $about_title; ?>
                        </h3>
                    <?php endif; ?>

                    <?php $about_subtitle = get_field('about_subtitle'); ?>
                    <?php if ( $about_subtitle ) : ?>
                        <div class="subtitle">
                            <?php echo $about_subtitle; ?>
                        </div>
                    <?php endif; ?>

                    <?php the_field('about_text_top'); ?>

                    <?php
                    $about_insta_link = get_field('about_instagram_link');
                    if( $about_insta_link ):
                        $link_url = $about_insta_link['url'];
                        $link_title = $about_insta_link['title'];
                        $link_target = $about_insta_link['target'] ? $about_insta_link['target'] : '_self';
                        ?>
                        <a class="about-block-insta_string" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">
                            <img width="31" height="31" src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/03_Insta.svg" alt="">
                            <?php echo esc_html( $link_title ); ?>
                        </a>
                    <?php endif; ?>

                    <?php the_field('about_text_bottom'); ?>
                </div>
            </div>
        </section>

        <section id="my_beliefs" class="my_beliefs">
            <div class="container wow fadeIn">
                <?php $beliefs_title = get_field('beliefs_title'); ?>
                <?php if ( $beliefs_title ) : ?>
                    <h3 class="text-uppercase">
                        <?php echo $beliefs_title; ?>
                    </h3>
                <?php endif; ?>

                <?php $beliefs_text = get_field('beliefs_text'); ?>
                <?php if ( $beliefs_text ) : ?>
                    <div class="description-text">
                        <?php echo $beliefs_text; ?>
                    </div>
                <?php endif; ?>

            </div>
        </section>

        <section id="main_target" class="main_target">
            <div class="container wow fadeIn">
                <?php $goals_title = get_field('goals_title'); ?>
                <?php if ( $goals_title ) : ?>
                    <h3 class="text-uppercase">
                        <?php echo $goals_title; ?>
                    </h3>
                <?php endif; ?>

                <?php $goals = get_field('goals'); ?>
                <?php if ( $goals ) : ?>

                    <div class="flex flex-wrap">

                        <?php foreach ( $goals as $goal ) : ?>
                            <div class="main_target-block flex f-d-column">
                                <img src="<?php echo $goal['icon']; ?>" alt="">
                                <div class="description-text">
                                    <?php echo $goal[ 'text' ]; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>

                    </div>

                <?php endif; ?>
            </div>
        </section>

        <section id="before_marathon" class="after_marathon  marathon-who">
            <div class="container wow fadeIn">
                <?php $w_marathon_title = get_field('w_marathon_title'); ?>
                <?php if ( $w_marathon_title ) : ?>
                    <h3 class="text-uppercase">
                        <?php echo $w_marathon_title; ?>
                    </h3>
                <?php endif; ?>

                <?php
                $w_marathon_list_l = get_field('w_marathon_list_l');
                $w_marathon_list_r = get_field('w_marathon_list_r');
                ?>
                <div class="flex after_marathon-decisions">
                    <?php if ( $w_marathon_list_l ) : ?>
                        <div class="after_marathon-block">
                            <ul>
                                <?php foreach ( $w_marathon_list_l as $w_marathon_list_li ) : ?>
                                    <li>
                                        <?php echo $w_marathon_list_li[ 'w_marathon_list_item_l' ]; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if ( $w_marathon_list_r ) : ?>
                        <div class="after_marathon-block">
                            <ul>
                                <?php foreach ( $w_marathon_list_r as $w_marathon_list_ri ) : ?>
                                <li>
                                    <?php echo $w_marathon_list_ri[ 'w_marathon_list_item_r' ]; ?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </section>

        <section id="after_marathon" class="after_marathon">
            <div class="container wow fadeIn">
                <?php $benefits_title = get_field('benefits_title'); ?>
                <?php if ( $benefits_title ) : ?>
                    <h3 class="text-uppercase">
                        <?php echo $benefits_title; ?>
                    </h3>
                <?php endif; ?>

                <?php
                $benefits_left = get_field('benefits_left');
                $benefits_right = get_field('benefits_right');
                ?>
                <div class="flex after_marathon-decisions">
                    <?php if ( $benefits_left ) : ?>
                        <div class="after_marathon-block">
                            <ul>
                                <?php foreach ( $benefits_left as $benefit_left ) : ?>
                                    <li>
                                        <?php echo $benefit_left[ 'benefit' ]; ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if ( $benefits_right ) : ?>
                        <div class="after_marathon-block">
                            <ul>
                                <?php foreach ( $benefits_right as $benefit_right ) : ?>
                                <li>
                                    <?php echo $benefit_right[ 'benefit' ]; ?>
                                </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </section>

        <section id="tasks" class="tasks">
            <div class="container">
                <?php $program_title = get_field('program_title'); ?>
                <?php if ( $program_title ) : ?>
                    <h3 class="text-uppercase text-center wow fadeIn">
                        <?php echo $program_title; ?>
                    </h3>
                <?php endif; ?>

                <?php $program_items = get_field('program'); ?>
                <?php if ( $program_items ) : ?>
                    <?php foreach ( $program_items as $i => $program_item ) : ?>
                        <div class="tasks-item flex wow fadeIn">
                            <div class="tasks-item_block tasks-item_title">
                                <div class="num">
                                    <?php echo ( $i < 9 ) ? '0'.( $i + 1 ) : ( $i + 1 ); ?>
                                </div>
                                <?php echo $program_item[ 'title' ]; ?>
                            </div>
                            <div class="tasks-item_block tasks-item_description">
                                <?php echo $program_item[ 'content' ]; ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </section>

        <section id="price" class="price">
            <div class="container flex wow fadeIn">
                <?php if ( $event_date || $event_telegram ) : ?>
                    <div class="main_section-event_info">
                        <table class="">
                            <?php if ( $event_date ) : ?>
                                <tr>
                                    <td>
                                        <img width="20" height="20" src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/13_Calendar_small_blue.svg" alt="">
                                    </td>
                                    <td>
                                        <?php echo $event_date; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>

                            <?php if ( $event_telegram ) : ?>
                                <tr>
                                    <td>
                                        <img width="22" height="20" src="<?php echo get_template_directory_uri() ?>/assets/img/marathon/14_Telegram_small_blue.svg" alt="">
                                    </td>
                                    <td>
                                        <?php echo $event_telegram; ?>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </table>
                    </div>
                <?php endif; ?>

                <div class="price_block main_section-event_info">
                    <table>
                        <?php $price_title = get_field('price_title'); ?>
                        <?php if ( $price_title ) : ?>
                            <tr>
                                <td colspan="4" class="t-head">
                                    <?php echo $price_title; ?>
                                </td>
                            </tr>
                        <?php endif; ?>

                        <?php
                        $package_price_1 = get_field('package_1_price');
                        $package_descrip_1 = get_field('package_1_description');
                        $package_price_2 = get_field('package_2_price');
                        $package_descrip_2 = get_field('package_2_description');
                        $package_price_3 = get_field('package_3_price');
                        $package_descrip_3 = get_field('package_3_description');
                        $package_price_4 = get_field('package_4_price');
                        $package_descrip_4 = get_field('package_4_description');
                        ?>
                        <tr>
                            <?php if ( $package_price_1 ) : ?>
                                <td>
                                    <div class="cost">
                                        <?php echo $package_price_1; ?>
                                    </div>
                                    <?php echo $package_descrip_1; ?>
                                </td>
                            <?php endif; ?>

                            <?php if ( $package_price_2 ) : ?>
                                <td>
                                    <div class="cost">
                                        <?php echo $package_price_2; ?>
                                    </div>
                                    <?php echo $package_descrip_2; ?>
                                </td>
                            <?php endif; ?>

                            <?php if ( $package_price_3 ) : ?>
                                <td>
                                    <div class="cost">
                                        <?php echo $package_price_3; ?>
                                    </div>
                                    <?php echo $package_descrip_3; ?>
                                </td>
                            <?php endif; ?>

                            <?php if ( $package_price_4 ) : ?>
                                <td>
                                    <div class="cost">
                                        <?php echo $package_price_4; ?>
                                    </div>
                                    <?php echo $package_descrip_4; ?>
                                </td>
                            <?php endif; ?>
                        </tr>
                    </table>
                </div>
            </div>
        </section>

        <section id="register" class="register">
            <div class="container flex register-container wow fadeIn">
                <div class="register-text_block flex f-d-column j-c-space-between">
                    <?php $contacts_title = get_field('contacts_title'); ?>
                    <?php if ( $contacts_title ) : ?>
                        <div class="register_title c-white">
                            <?php echo $contacts_title; ?>
                        </div>
                    <?php endif; ?>

                    <?php $contacts_description = get_field('contacts_description'); ?>
                    <?php if ( $contacts_description ) : ?>
                        <div class="register-questions c-white">
                            <?php echo $contacts_description; ?>
                        </div>
                    <?php endif; ?>
                </div>

                <?php $contacts_form_shortcode = get_field('contacts_form_shortcode'); ?>
                <?php if ( $contacts_form_shortcode ) : ?>
                    <div class="register-rorm_block">
                        <?php echo do_shortcode($contacts_form_shortcode); ?>
                    </div>
                <?php endif; ?>
            </div>
        </section>

    </div>

<?php get_footer(); ?>