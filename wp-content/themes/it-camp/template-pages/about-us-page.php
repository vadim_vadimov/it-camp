<?php 
/**
 *	Template name: About us Page 
 */

get_header(); ?>

        <div class="intro  intro--inner  intro--inner-small">
            <div class="container">

                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <h1><?php esc_html( the_title() ) ?></h1>

                <?php endwhile; ?>
                <?php endif; ?> 

            </div>
        </div>
    </div>

    <section class="who-we-are  about-us-inner">
        <div class="container">

            <?php if (get_field('about_intro_title')) { ?>
                <h3 class="main-title-about"><?php the_field('about_intro_title') ?></h3>
            <?php } ?>  

            <div class="who-we-are__content">

                <?php if (get_field('about_intro_texts')) { ?>
                    <div class="who-we-are__info">
                        <?php the_field('about_intro_texts') ?>
                    </div>
                <?php } ?>  

                <?php if (get_field('about_intro_main_img')) { ?>
                    <div class="who-we-are__info-img">
                        <img src="<?php the_field('about_intro_main_img') ?>">
                    </div>
                <?php } ?>  

            </div>
        </div>
    </section>

    <section class="about-us-main">
        <div class="container">

            <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                <div class="about-us-main__content">
                    <?php esc_html( the_content() ) ?>
                </div>

            <?php endwhile; ?>
            <?php endif; ?> 

            <?php if (get_field('about_slogan_text')) { ?>
                <p class="about-us-main__bottom-text"><?php the_field('about_slogan_text') ?></p>
            <?php } ?>  

        </div>
    </section>

    <section class="trusted">
        <div class="container">
            <?php if (get_field('trusted_title', apply_filters( 'wpml_object_id', 18, 'page', TRUE  ) )) { ?>
                <h2 class="main-title"><?php the_field('trusted_title', apply_filters( 'wpml_object_id', 18, 'page', TRUE  ) ) ?></h2>
            <?php } ?>  

            <?php if( have_rows('trusted_list', apply_filters( 'wpml_object_id', 18, 'page', TRUE  ) ) ): ?>
                <div class="trusted__list">
                    <?php while( have_rows('trusted_list', apply_filters( 'wpml_object_id', 18, 'page', TRUE  ) ) ): the_row();
                        $img = get_sub_field('trusted_list_img');

                        ?>

                        <div class="trusted__item-wrap">
                            <div class="trusted__item">
                                <img src="<?php echo $img; ?>">
                            </div>
                        </div>

                    <?php endwhile; ?>  
                </div>
            <?php endif; ?> 

        </div>
    </section>

    <section class="help-panel">
        <div class="container">
            <div class="help-panel__content">

                <?php if (get_field('about_help_sub_title')) { ?>
                    <h3 class="help-panel__sub-title"><?php the_field('about_help_sub_title') ?></h3>
                <?php } ?> 

                <?php if (get_field('about_help_title')) { ?>
                    <h2 class="help-panel__title form-info__title"><?php the_field('about_help_title') ?></h2>
                <?php } ?> 


                <div class="help-panel__about-btn">
                    <a href="<?php echo esc_url( get_page_link( 197 ) ); ?>" class="help-panel__about-link"><?php _e('CONTACT US', 'itcamp'); ?></a>
                </div>
            </div>
        </div>
    </section>
    

<?php get_footer(); ?>