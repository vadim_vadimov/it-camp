<?php 
/**
 *	Template name: Referral Program Page 
 */

get_header(); ?>

    <section class="referral-program">
        <div class="container">

            <?php if (get_field('referral_title')) { ?>
                <h2 class="main-title"><?php the_field('referral_title') ?></h2>
            <?php } ?>  

            <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                <div class="referral-program__content">
                    <?php esc_html( the_content() ) ?>
                </div>

            <?php endwhile; ?>
            <?php endif; ?> 

            <div class="referral-program__info">

                <?php if (get_field('refferal_info_text')) { ?>
                    <p><?php the_field('refferal_info_text') ?></p>
                <?php } ?> 

                <?php if (get_field('refferal_info_tel')) { ?>
                    <div class="referral-program__info-item">
                        <p><?php _e('please call:', 'itcamp'); ?></p>
                        <a href="tel:<?php the_field('refferal_info_tel') ?>"><?php the_field('refferal_info_tel') ?></a>
                    </div>
                <?php } ?> 

                <?php if (get_field('refferal_info_mail')) { ?>
                    <div class="referral-program__info-item">
                        <p><?php _e('or email:', 'itcamp'); ?></p>
                        <a href="mailto:<?php the_field('refferal_info_mail') ?>"><?php the_field('refferal_info_mail') ?></a>
                    </div>
                <?php } ?> 

            </div>
        </div>
    </section>

<?php get_footer(); ?>