<section class="form-info" id="contact-us-form" style="background-image: url(<?php the_field('lets_talk_bg', 'options') ?>);">
    <div class="container">
        <div class="form-info__content">
            <div class="form-info__title-content">

                <?php if (get_field('lets_talk_title', 'options')) { ?>
                    <h2 class="form-info__title"><?php the_field('lets_talk_title', 'options') ?></h2>
                <?php } ?>  
                
                <?php if (get_field('lets_talk_title_text', 'options')) { ?>
                    <?php the_field('lets_talk_title_text', 'options') ?>
                <?php } ?>  
            </div>

            <?php if (get_field('lets_talk_form', 'options')) { ?>
                <?php the_field('lets_talk_form', 'options') ?>
            <?php } ?>  

        </div>
    </div>
</section>