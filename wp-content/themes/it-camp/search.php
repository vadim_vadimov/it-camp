<?php get_header(); ?>

<section class="search-inner">
	<div class="container">
		<h2 class="main-title">Searching results</h2>
		
		<?php
		    global $query_string;
		    $query_args = explode("&", $query_string);
		    $search_query = array('post_type' => 'blog',
		    					'posts_per_page' => 40,
		    					'order' => 'DESC');

		    foreach($query_args as $key => $string) {
		      $query_split = explode("=", $string);
		      $search_query[$query_split[0]] = urldecode($query_split[1]);
		    } // foreach

		    $page_index = new WP_Query($search_query);
		    if ( $page_index->have_posts() && get_search_query() ) : 
		    ?>

		    <!-- the loop -->

		    <div class="posts__list">

		    	<?php while ( $page_index->have_posts() ) : $page_index->the_post(); ?>

		    		<div class="posts__item-wrap">
		    			<a href="<?php echo esc_url( get_permalink() ); ?>" class="posts__item">
		    				<div class="posts__item-img" style="background-image: url(<?php the_field('blog_main_img') ?>);">

		    					<?php
		    					$cats = get_the_category();
		    					for ($i = 0; $i < count($cats); $i++) {
		    						echo '<span class="posts__category">' . $cats[ $i ]->cat_name . '</span>';
		    					}?>

		    				</div>
		    				<div class="posts__item-info">
		    					<h4><?php echo esc_html( the_title() ); ?></h4>
		    					<?php echo esc_html( the_excerpt() ); ?>
		    				</div>
		    				<div class="posts__item-arrow">
		    					<span class="posts__item-arrow-text">read more</span>
		    					<img src="<?php echo get_template_directory_uri() ?>/assets/img/post-arr.svg">
		    				</div>
		    			</a>
		    		</div>

		    	<?php endwhile; ?>
		    		
		    </div>

		    <!-- end of the loop -->

		    <?php wp_reset_postdata(); ?>


		<?php else : ?>

			<?php get_search_form(); ?>

			<div class="main-title__wrap--search">
			    <h4 class="main-sub-title"><?php _e( 'No results were found.Please try again' ); ?></h4>
			</div>

		<?php endif; ?>

	</div>
	
</section>
	
<?php get_footer(); ?>