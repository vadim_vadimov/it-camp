<?php

get_header(); ?>


<?php if ( have_posts() ) : ?>
	<?php while ( have_posts() ) : the_post(); ?>

        <section class="referral-program">
            <div class="container">
                <h2 class="main-title">
					<?php the_title(); ?>
                </h2>

                <div class="referral-program__content">
                    <?php the_content(); ?>
                </div>

            </div>
        </section>

	<?php endwhile; ?>
<?php endif; ?>


<?php get_footer(); ?>