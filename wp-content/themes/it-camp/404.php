<?php get_header(); ?>
	
	<div class="page-zero">
		<div class="container">
			<span class="page-zero__title">404</span>
			<p class="page-zero__text">Page not found</p> 
			<div class="page-zero__link-wrap">
				<a href="<?php echo esc_url( home_url() ); ?>" class="page-zero__link">BACK TO HOMEPAGE</a>
			</div>
		</div>
	</div>
	
<?php get_footer(); ?>