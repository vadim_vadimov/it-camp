<?php get_header(); ?>

    <section class="content-cols">
        <div class="container">
            <div class="content-cols__info-content">

                <span class="link-back-wrap">
                    <a href="<?php echo get_page_link(129); ?>" class="link-back">
                        <span class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.25 10.28">
                                <polygon fill="#0e2766" points="19.25 4.54 2.28 4.54 5.98 0.84 5.14 0 0 5.14 5.14 10.28 5.98 9.44 2.28 5.74 19.25 5.74 19.25 4.54"/>
                            </svg>
                        </span>
			            <?php _e('Back', 'itcamp'); ?>
                    </a>
                </span>

                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <h2 class="main-title main-title--single-cv"><?php esc_html( the_title() ) ?></h2>

                <?php endwhile; ?>
                <?php endif; ?> 

                <?php if (get_field('profile_text')) { ?>
                    <div class="content-cols__cv-content">
                        <h4><?php _e('Profile', 'itcamp'); ?>:</h4>
                        <?php the_field('profile_text') ?>
                    </div>
                <?php } ?>  

                
                <?php if( have_rows('work_experience_list') ): ?>   
                    <div class="content-cols__cv-content">
                        <h4><?php _e('Work Experience', 'itcamp'); ?>:</h4>
                        <div class="content-cols__cv-list">
                            <?php while( have_rows('work_experience_list') ): the_row(); 
                                $date = get_sub_field('work_experience_list_dates');
                                $position = get_sub_field('work_experience_list_text');
                                $company = get_sub_field('work_experience_list_company');
                                $geo = get_sub_field('work_experience_list_geo');

                                ?>

                                <div class="content-cols__cv-item">
                                    <span class="content-cols__cv-date"><?php echo $date; ?></span>
                                    <div class="content-cols__cv-item-info">
                                        <p><?php echo $position; ?></p>
                                        <p><?php echo $company; ?></p>
                                        <p><?php echo $geo; ?></p>
                                    </div>
                                </div>

                            <?php endwhile; ?>  
                        </div>
                    </div>
                <?php endif; ?> 

                <?php if( have_rows('education_list') ): ?>   
                    <div class="content-cols__cv-content">
                        <h4><?php _e('Education', 'itcamp'); ?>:</h4>
                        <div class="content-cols__cv-list">
                            <?php while( have_rows('education_list') ): the_row(); 
                                $date = get_sub_field('education_list_date');
                                $place = get_sub_field('education_list_education_place');
                                $education = get_sub_field('education_list_education');

                                ?>

                                <div class="content-cols__cv-item">
                                    <span class="content-cols__cv-date"><?php echo $date; ?></span>
                                    <div class="content-cols__cv-item-info">
                                        <p><?php echo $place; ?></p>
                                        <p><?php echo $education; ?></p>
                                    </div>
                                </div>

                            <?php endwhile; ?>  
                        </div>
                    </div>
                <?php endif; ?> 
                
                <?php if (get_field('skills_block_title')) { ?>
                    <div class="content-cols__cv-content">
                        <h4><?php _e('Skills', 'itcamp'); ?>: </h4>
                        <?php the_field('skills_block_title') ?>
                    </div>
                <?php } ?>  

            </div>
            <div class="content-cols__form-content">

	            <?php $form_title = get_field('cv_form_title', 'option'); ?>
	            <?php if ($form_title) { ?>
                    <h3 class="content-cols__form-content-title"><?php echo $form_title; ?></h3>
	            <?php } ?>

                <?php $form_subtitle = get_field('cv_form_subtitle', 'option'); ?>
	            <?php if ($form_subtitle) { ?>
                    <p class="content-cols__form-content-sub-title"><?php echo $form_subtitle; ?></p>
	            <?php } ?>

	            <?php $form_shortcode = get_field('cv_form_shortcode', 'option'); ?>
	            <?php if ($form_shortcode) { ?>
		            <?php echo do_shortcode($form_shortcode); ?>
	            <?php } ?>

            </div>
        </div>
    </section>

<?php get_footer(); ?>