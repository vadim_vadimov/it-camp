<form class="posts-inner__search-form" role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>">
	<div class="posts-inner__search-field">
		<input type="text" value="<?php echo get_search_query() ?>" name="s" id="s"  placeholder="START TYPING">
		<button id="searchsubmit" class="posts-inner__search-submit">
			<img src="<?php echo get_template_directory_uri() ?>/assets/img/lupa-h.svg">
		</button>
	</div>
	<div class="posts-inner__search-btn"></div>
	<div class="posts-inner__search-close"></div>
</form>