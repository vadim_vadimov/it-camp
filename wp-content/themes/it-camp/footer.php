<?php if (!is_page_template( 'template-pages/marathon-page.php' )) { ?>

    <footer class="footer <?php if (is_404()) { ?> footer-404 <?php }?>">
        <div class="container-big">

            <?php if (get_field('footer_copyright', 'options')) { ?>
                <p class="footer__copyright"><?php the_field('footer_copyright', 'options') ?></p>
            <?php } ?>

            <?php if( have_rows('footer_social_list', 'options') ): ?>
                <div class="footer__socials">
                    <?php while( have_rows('footer_social_list', 'options') ): the_row();
                        $text = get_sub_field('footer_social_list_item_name');
                        $link = get_sub_field('footer_social_list_item_link');

                        ?>

                        <a href="<?php echo $link; ?>" target="_blank" class="footer__social-item"><?php echo $text; ?></a>

                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

            <p class="footer__design"><?php _e('Made by', 'itcamp'); ?> <a href="https://case-digital.com/" target="_blank">CASE Digital Studio</a></p>
        </div>
    </footer>

<?php } ?>


	<?php wp_footer(); ?>

</body>
</html>