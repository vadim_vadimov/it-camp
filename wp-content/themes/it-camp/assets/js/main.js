$(function () {
    let $introTitleSlider = $('.intro-title-slider');
    if ($introTitleSlider.length) {
        $introTitleSlider.slick({
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            arrows: false,
            dots: false,
            pauseOnFocus: false,
            pauseOnHover: false,
            pauseOnDotsHover: false,
            autoplay: true,
            autoplaySpeed: 3000
        })
    }



    function processSLider($slider) {
        let $svg = $('.process-slider__svg.active'),
            $path = $svg.find('path');

        let tl = new TimelineMax({paused: true});
        tl.fromTo($path, 6, {drawSVG: '100% 0'}, {drawSVG: 0, ease: Power0.easeInOut});
        function loaderAnimStart () {
            tl.play(0);
        }
        $slider.on('init', function () {
            loaderAnimStart();
        });
        $slider.slick({
            slidesToShow: 1,
            infinite: true,
            dots: true,
            arrows: false,
            speed: 700,
            slidesToScroll: 1,
            fade: true,
            cssEase: 'linear',
            pauseOnFocus: false,
            pauseOnHover: false,
            pauseOnDotsHover: false,
            autoplay: true,
            autoplaySpeed: 6000
        });
        $slider.on('beforeChange', function () {
            loaderAnimStart();
        });
    }
    let $processSlider = $('.process__slider');
    if ($processSlider.length) {
        processSLider($processSlider);
    }

    $('input[type="file"]').change(function(){
        let $form = $(this).closest('form');
        let value = $form.find("input[type='file']").val();
        $form.find('.input-file-label').text(this.files[0].name);
        $form.find('.main-form__label-file').addClass('active');
        $form.find('.input-file-label-close').fadeIn(function() {
            $(this).addClass('visible')
        });
    });
    $('.input-file-label-close').on('click', function (e) {
        e.preventDefault();
        let $this = $(this),
            $form = $this.closest('form'),
            $fileInput = $form.find('input[type="file"]'),
            $inputLabel = $form.find('.main-form__label-file'),
            $inputLabelText = $inputLabel.find('.input-file-label')
        $fileInput.val('');
        $inputLabel.removeClass('active');
        $inputLabelText.text($inputLabelText.attr('data-text-default'));
        $this.removeClass('visible').fadeOut();
    });


    let wpcf7Elm = document.querySelector( '.wpcf7' );
    if (wpcf7Elm) {
        wpcf7Elm.addEventListener( 'wpcf7submit', function( event ) {
            let $inputFileLabel = $(wpcf7Elm).find('.input-file-label');
            if ($inputFileLabel.length) {
                $inputFileLabel.html($inputFileLabel.attr('data-text-default'))
                $(wpcf7Elm).find('.main-form__label-file').removeClass('active');
            }

            let $fileInputClose = $(wpcf7Elm).find('.input-file-label-close');
            if ($fileInputClose.length) {
                $fileInputClose.removeClass('visible').fadeOut();
            }
        }, false );
    }


    $('.header__burger').on('click', function () {
      $('.header__menu').addClass('active');
      $('body').addClass('overflowHidden');
    });

    $('.header__menu-close').on('click', function () {
      $('.header__menu').removeClass('active');
      $('body').removeClass('overflowHidden');
    });

    $('.posts-inner__search-close').hide();

    $('.posts-inner__search-btn').on('click', function () {
      $('.posts-inner__search-field').addClass('active');
      $('.posts-inner__nav-list').addClass('hidden');
      $('.posts-inner__search-close').show();
      $(this).hide();
    });

     $('.posts-inner__search-close').on('click', function () {
      $('.posts-inner__search-field').removeClass('active');
      $('.posts-inner__nav-list').removeClass('hidden');
      $('.posts-inner__search-btn').show();
      $(this).hide();
    });



    if ($(window).width() < 739) {
      $('.posts-inner__search-btn').on('click', function () {
        $('.posts-inner__nav-list').removeClass('hidden');
      });


    }


    $('.slider-for').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false,
      fade: true,
      infinite: false,
      asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
      slidesToShow: 5,
      slidesToScroll: 1,
      asNavFor: '.slider-for',
      dots: false,
      arrows: false,
      infinite: false,
      focusOnSelect: true
    });




    function fixedHeader () {
        let $header = $('header.header');
        if ($header.length === 0) return false;
        let $headerGap = $('<div class="page-header-gap"></div>').insertAfter($header);

        let headerController = new ScrollMagic.Controller();
        let headerScene = new ScrollMagic.Scene({
            offset: $header.outerHeight()*6
        })
            .addTo(headerController);

        function fixHeader() {

            let tl = new TimelineMax();
            tl.add(function () {
                let headerH = $header.outerHeight();
                $headerGap.css('height', headerH);
                $header.addClass('header--fixed');
            });
            tl.fromTo($header, 0.6, {yPercent: -100}, { yPercent: 0, ease: 'power3.inOut' })
        }
        function unfixHeader() {

            let tl = new TimelineMax();
            tl.fromTo($header, 0.1, {yPercent: -0}, { yPercent: -100, ease: 'power3.inOut' });
            tl.add(function () {
                $headerGap.css('height', 0);
                $header.removeClass('header--fixed').attr('style', '');
            });
        }

        headerScene.on('enter', function (e) {
            if (e.scrollDirection === 'FORWARD') {
                fixHeader();
            }
        });
        headerScene.on('leave', function(e) {
            if (e.scrollDirection === 'REVERSE') {
                unfixHeader();
            }
        });
    }
    fixedHeader();



    new WOW().init();


    $("a.scrollTo").click(function () {
        let elementClick = $(this).attr("href"),
            destination = $(elementClick).offset().top;
        $("html:not(:animated),body:not(:animated)").animate({scrollTop: destination}, 1100);
        return false;
    });


    let $servicesForm = $('.services-form-info .wpcf7');
    if ($servicesForm.length) {
        $servicesForm.find('select').on('change', function() {
            let $this = $(this);
            if ( $this.val() ) {
                $this.addClass('selected');
            } else {
                $this.removeClass('selected');
            }
        })
    }
});