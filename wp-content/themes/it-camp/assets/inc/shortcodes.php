<?php

add_shortcode( 'price', 'shortcode_price' );
function shortcode_price ($atts, $content) {
    return '<span class="price">' . $content . '</span>';
}