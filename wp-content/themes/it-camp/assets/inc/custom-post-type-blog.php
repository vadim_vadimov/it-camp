<?php

// Register Custom Post Type
function custom_post_type_blog() {

	$labels = array(
		'name'                  => 'Blog',
		'singular_name'         => 'Blog',
		'menu_name'             => 'Blog',
		'add_new_item'          => 'Add New',
		'add_new'               => 'Add New',
		'new_item'              => 'New',
		'edit_item'             => 'Edit',
		'update_item'           => 'Update',
		'view_item'             => 'View',
		'view_items'            => 'View All',
	);
	$rewrite = array(
		'slug'                  => 'blog',
		'with_front'            => true,
		'pages'                 => false,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => 'Blog',
		'description'           => 'It-Camp blog',
		'labels'                => $labels,
		'supports'              => array(
			'title',
			'editor'
		),
		'taxonomies'  => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'show_in_rest' => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-list-view',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page'
	);
	register_post_type( 'blog', $args );

}
add_action( 'init', 'custom_post_type_blog', 0 );


?>