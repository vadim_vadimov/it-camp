<?php

// Register Custom Post Type
function custom_post_type_vacancy() {

	$labels = array(
		'name'                  => 'Vacancy',
		'singular_name'         => 'Vacancy',
		'menu_name'             => 'Vacancy',
		'add_new_item'          => 'Add New',
		'add_new'               => 'Add New',
		'new_item'              => 'New',
		'edit_item'             => 'Edit',
		'update_item'           => 'Update',
		'view_item'             => 'View',
		'view_items'            => 'View All',
	);
	$rewrite = array(
		'slug'                  => 'vacancy',
		'with_front'            => true,
		'pages'                 => false,
		'feeds'                 => false,
	);
	$args = array(
		'label'                 => 'Vacancy',
		'description'           => 'It-Camp vacancy',
		'labels'                => $labels,
		'supports'              => array(
			'title',
			'editor'
		),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 6,
		'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'rewrite'               => $rewrite,
		'capability_type'       => 'page'
	);
	register_post_type( 'vacancy', $args );

}
add_action( 'init', 'custom_post_type_vacancy', 0 );


?>