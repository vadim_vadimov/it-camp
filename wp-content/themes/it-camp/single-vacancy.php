<?php get_header(); ?>

    <section class="content-cols">
        <div class="container">
            <div class="content-cols__info-content">

                <span class="link-back-wrap">
                    <a href="<?php echo get_page_link(177); ?>" class="link-back">
                        <span class="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 19.25 10.28">
                                <polygon fill="#0e2766" points="19.25 4.54 2.28 4.54 5.98 0.84 5.14 0 0 5.14 5.14 10.28 5.98 9.44 2.28 5.74 19.25 5.74 19.25 4.54"/>
                            </svg>
                        </span>
		                <?php _e('Back', 'itcamp'); ?>
                    </a>
                </span>


                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <h2 class="main-title main-title--single-vacancy"><?php esc_html( the_title() ) ?></h2>

                <?php endwhile; ?>
                <?php endif; ?> 

                <?php if (get_field('vacancy_single_intro_tag')) { ?>
                    <p class="content-cols__tag"><?php the_field('vacancy_single_intro_tag') ?></p>
                <?php } ?>  
                
                <?php if ( have_posts() ) :  while ( have_posts() ) : the_post(); ?>

                    <?php esc_html( the_content() ) ?>

                <?php endwhile; ?>
                <?php endif; ?> 
                
            </div>
            <div class="content-cols__form-content">

                <?php $form_title = get_field('vacancy_form_title', 'option'); ?>
                <?php if ($form_title) { ?>
                    <h3 class="content-cols__form-content-title"><?php echo $form_title; ?></h3>
                <?php } ?>

                <?php $form_shortcode = get_field('vacancy_form_shortcode', 'option'); ?>
                <?php if ($form_shortcode) { ?>
                    <?php echo do_shortcode($form_shortcode); ?>
                <?php } ?>

            </div>
        </div>
    </section>

<?php get_footer(); ?>